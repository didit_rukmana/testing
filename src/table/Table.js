import React, { useEffect, useState, Component } from 'react';


import ReactTable from 'react-table-6'
import 'react-table-6/react-table.css'

import axios from 'axios';

const Table = (props) => {
      const [data, setData] = useState([])
              useEffect(()=>{
                  axios.get(`https://jsonplaceholder.typicode.com/posts`)
                  .then(res => setData(res.data) )
                  .catch(err => console.log(err));
              },[])
     
      const columns = [
              {
                  Header: 'userId',
                  accessor: 'userId' // String-based value accessors!
              }
              , {
                  Header: 'id',
                  accessor: 'id',
                  // Cell: props => <span className='number'>{props.value}</span> // Custom cell components!
              } , {
                Header: 'title',
                accessor: 'title',
                // Cell: props => <span className='number'>{props.value}</span> // Custom cell components!
            },  {
              Header: 'body',
              accessor: 'body',
              // Cell: props => <span className='number'>{props.value}</span> // Custom cell components!
          }, ]

  

      console.log('wqwq',[0].title)
      return (
          <div>
              <ReactTable
                data={data}
                columns={columns}
              />
          </div>
    )
}

export default Table;