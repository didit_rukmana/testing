import React from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css'


class GridLibraries extends React.Component {
    _isMounted = false;

    constructor(props) {
        super(props);

        this.state = {
            pages: -1,
            loading: true,
            sorted: []
        };
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    render() {
        return (
            <ReactTable 
                columns={this.props.columns} 
                data={this.props.datasrc}
                pages={Math.ceil(this.props.countdata / this.state.pages)} // should default to -1 (which means we don't know how many pages we have)
                loading={this.state.loading}
                defaultPageSize={25}
                pageSizeOptions={[25, 50, 75, 100]}
                filterable
                manual // informs React Table that you'll be handling sorting and pagination server-side
                style={{ height: 400 }}
                // className="-striped -highlight"
                onFetchData={(state, intance) => { 
                    this._isMounted = true;

                    if (this._isMounted) { 
                        this.setState({ loading: true })
                        this.props.requestData(state, intance).then(() => {
                            this.setState({
                                pages: state.pageSize,
                                loading: false
                            })
                        })
                    }
                }}
                sorted={this.state.sorted}
                onSortedChange={(newSort, column) => {
                    if (this._isMounted) { 
                        this.setState({ sorted: newSort });
                    }
                }}
            >  
            </ReactTable>  
        )
    }
}


export default GridLibraries;


/* End of file GridLibraries.js */
/* Location: ./src/components/libraries/GridLibraries.js */